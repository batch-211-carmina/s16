console.log("Hello World");

//Arithmetic Operators

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	//modulo use also for divisibility
	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);


//Assignment Operators

	//Basic Assignment Operator

	let assignmentNumber = 8;

	//Addition Assignment Operator
	//Addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment operator:" + assignmentNumber);


	//shorthand
	assignmentNumber += 2;
	console.log("Result of addition assignment operator:" + assignmentNumber);

	//Subraction | Multiplication | Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator:" + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator:" + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator:" + assignmentNumber);


//Multiple Operators and Parentheses

	/*
		- when multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition, and Subraction) rule.
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/

	let mdas = 1 +2 - 3 * 4 /5;
	console.log("Result of mdas operation: " + mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: " + pemdas);

	/*
		- By adding parentheses "()" to create more complex computations wil lchange the order of operations still ff. the same rule
		1. 4/5 = 0.8
		2. 2-3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = .19
	*/


//Increment and Decrement
	
	//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment or decrement was applied to.

	let z = 1;
	//the value of 'z' is added by a value of one before returning the value and storing it in the variable 'increment'.
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	//The value of 'z' is returned and stored in the variable 'increment' then the value of 'z' is increased by one

	increment = z++;
	//The value of 'z' is at 2 before it was incremented
	console.log("Result of post-increment: " + increment);
	//The value of 'z' was increased again reassigning the value to 3

	console.log("Result of post-increment: " + z);

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);


//Type Coercion

	let numA = '10';
	let numB = 12;

	/*
		- adding/concatenating a string and a number will result in a string
		- this can be proven in the console by looking at the color of the text displayed
	*/

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	/*
		- The result is a number
		- This can be proven in the console by looking at the color of the text displayed
		- blue/violet text means that the output returned is a number data type
	*/
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);


	/*
		- The result is a number
		- The boolean 'true' is also associated with the value of 1
	*/
	let numE = true + 1;
	console.log(numE);


	/*
		- The result is a number
		- The boolean 'false' is also associated with the value of 0
	*/
	let numF = false + 1;
	console.log(numF);


//Comparison Operators

	let juan = 'juan';

	//Equality Operator (==)
	/*
		- checks whether the operands are equal/have the same content
		- attempts to CONVERT and COMPARE operands of different data types
		- returns a boolean value
	*/

	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(0 == false); //true
	console.log('juan' == 'juan'); //true
	console.log('juan' == juan); //true

	//Inequality Operator (!==)
	/*
		- checks whether the operands are not equal/have different content
		- attempts to CONVERT and COMPARE operands of diffent date types
	*/

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(0 != false); //false
	console.log('juan' != 'juan'); //false
	console.log('juan' != juan); //false
	

	//Strict Equality Operator
	/*
		- checks whether the operands are equal/have the same content
		- also it COMPARES the date type of 2 values
		- JavaScript is a loosely types language meaning that values of different data types can be stored in variables
		- strict equality operators are better to use in most cases to ensure that data types are correct
	*/
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1'); //false
	console.log(0 === false); //false
	console.log('juan' === 'juan'); //true
	console.log('juan' === juan); //true


	//Strict Inequality Operator (!==)
	/*
		- checks whether the operands are not equal/have the same content
		- also COMPARES the data types of 2 values
	*/

	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(0 !== false); //true
	console.log('juan' !== 'juan'); //false
	console.log('juan' !== juan); //false


//Relational Operators

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	let isLessThan = a < b;
	let isGTorEqual = a >= b;
	let isLTorEqual = a <= b;

	console.log(isGreaterThan); //false
	console.log(isLessThan);	//true
	console.log(isGTorEqual);	//false
	console.log(isLTorEqual);	//true

	let numStr = "30";
	//coercion to change the string to a number
	console.log(a > numStr);	//true
	console.log(b <= numStr);	//false

	let str = "twenty";
	console.log(b >= str);	//false
	//give us false since the string is not numeric
	//the string was converted to a number


//Logical Operator

	// &&(AND) Operator

	let isLegalAge = true;
	let isRegistered = false;

	//Logigal AND Operator (&& - Double Ampersand)
	//Return TRUE if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log('Result of logical AND operator: ' + allRequirementsMet);	//false

	// ||(OR) Operator
	//Logigal OR Operator (|| - Double Pipe)
	//Return TRUE if one of the operands are true

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log('Result of logical OR operator: ' + someRequirementsMet);	//true

	// !(NOT) Operator
	//Logigal NOT Operator (! - Exclamation Point)
	//returns the opposite value

	let someRequirementsNotMet = !isRegistered;
	console.log('Result of logical NOT operator: ' + someRequirementsNotMet);	//true

	//'typeof' operator is used to check the data type of a value/expresson and returns a string value of what the data type is






